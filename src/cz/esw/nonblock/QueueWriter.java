package cz.esw.nonblock;

import java.util.concurrent.Callable;

/**
 * @author Marek Cuchý
 */
public class QueueWriter implements Callable<Stat> {
	private final static int MASK = 0xFFFFF00;
	private final static int MULTIPLIER = 256;

	private final StringQueue queue;

	private final int count;
	private final int add;
	private final Stat stat = new Stat();

	public QueueWriter(int count, StringQueue list, float offerPortion) {
		this.count = count;
		this.queue = list;
		this.add = (int)((1f-offerPortion)*MULTIPLIER);
	}

	@Override
	public Stat call() throws Exception {
		long id = Thread.currentThread().getId();
		String obj = Long.toString(id);
		int a = 0;
		int b = 0;
		for (int i = 0; i < count; i++) {
			a += add;
			int q = a & MASK;
			if (b == q) {
 				if (queue.offer(obj)) {
					stat.offered();
				} else {
					stat.full();
				}
			} else {
				if (queue.poll() != null) {
					stat.polled();
				} else {
					stat.empty();
				}
				b = q;
			}
		}
		return stat;
	}
}
