package cz.esw.nonblock;

public interface StringQueue {
    boolean offer(String element);
    String poll();
}
